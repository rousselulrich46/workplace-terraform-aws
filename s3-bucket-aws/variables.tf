variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "iam_username" {
  type        = string
  description = "The IAM username to use"
  default     = "terraformforbucket"
}

variable "bucket_name" {
  description = "Name of the S3 bucket"
  type        = string
  default     = "prod"
}


variable "bucket_names" {
  type = list(string)
  default = [
    "bbykamzou",
    "roskamzou",
    "famillykamzou"
  ]
}

variable "bucket_tags" {
  description = "Tags for the S3 bucket"
  type        = map(string)
  default     = {
    Name        = "prod"
    Environment = "Production"
  }
}

variable "aws_access_key" {
  description = "AWS access key"
  type        = string
  default     = "AKIAXS2OSXQUZXR3T5UD"
}

variable "aws_secret_key" {
  description = "AWS secret key"
  type        = string
  default     = "uiOFEEjFPRxqobHwnKNIgG6dj78DGwXDue/aKgSy"
}
