provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

resource "aws_s3_bucket" "buckets" {
  for_each = toset(var.bucket_names)
  bucket   = each.key
  tags = {
    Name = each.key
  }
}

# Création d'une politique de seau S3
resource "aws_s3_bucket_policy" "bucket_policy" {
  for_each = aws_s3_bucket.buckets
  bucket   = each.value.id
  policy   = data.aws_iam_policy_document.bucket_policy_document[each.key].json
}

# Politique de seau S3 - pour autoriser l'accès aux objets d'un seau particulier - à attacher au seau
data "aws_iam_policy_document" "bucket_policy_document" {
  for_each = aws_s3_bucket.buckets

  statement {
    principals {
      type        = "AWS"
      identifiers = [aws_iam_user.user.arn]
    }
    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]
    resources = [
      each.value.arn,
      "${each.value.arn}/*",
    ]
  }
}